// Load application styles
import 'styles/index.scss'
// Load mocks
import main from 'mocks/menumain'
import top from 'mocks/menutop'
// Load directives
import VShowSlide from 'v-show-slide'

Vue.use(VShowSlide, {
  customEasing: {
    bounce: 'cubic-bezier(0.68, -0.55, 0.265, 1.55)'
  }
})

Vue.component('nav-recursive', {
  data: function () {
    return {
      toggled: false
    }
  },
  props: {
    'item': Object
  },
  template: '<li class="menu-item"><a :href="item.url">{{item.title}}</a><p class="menu-item-description" v-if="item.description">{{item.description}}</p><template v-if="item.children"><span class="sub-menu-toggle" @click="toggled = !toggled" :class="{toggled: toggled}"></span><ul class="sub-menu"><nav-recursive v-for="child in item.children" :item="child" :key="child.id" v-show-slide:400:bounce="toggled || $vuetify.breakpoint.mdAndUp"/></ul></template></li>',
})
Vue.component('expandable-box', {
  props: {
    'toggled': {
      type: Boolean,
      default: false
    },
    'id': {
      type: Number,
      default: NaN
    },
    'ease': {
      type: Boolean,
      default: false
    },
    'wide-toggle': {
      type: Boolean,
      default: false
    }
  },
  model: {
    prop: 'toggled',
    event: 'toggle'
  },
  methods: {
    emit: function () {
      this.$emit('toggle', Number.isNaN(this.id) ? !this.toggled : this.id)
    }
  },
  template: '<div class="collapse row wrap relative" :class="{toggled: toggled, wideToggle: wideToggle}"><slot name="label"></slot><span class="collapse__toggle" @click="emit"></span><div v-show-slide:400="toggled" class="collapse__collapsable" v-if="ease"><slot name="content"></slot></div><div v-show-slide:400:bounce="toggled" class="collapse__collapsable" v-else><slot name="content"></slot></div></div>'
})

var header = new Vue({
  el: 'header',
  data: {
    menutop: top,
    menumain: main,
    menustoggle: false
  },
  methods: {
    toggle: function(e) {
      var classes = e.target.parentElement.classList
      if (classes) {
          classes.toggle("toggled");
      } else {
          // For IE9
          var classesArray = classes.split(" ");
          var i = classesArray.indexOf("toggled");
          if (i >= 0)
              classesArray.splice(i, 1);
          else
              classesArray.push("toggled");
              e.target.parentElement.className = e.target.parentElement.join(" ");
      }
    }
  }
})

/*TODO: place in one instance FRONT */
var methodology = new Vue({
  el:'.methodology',
  data: {
    steps: [
      {
        title: 'Analiza potrzeb',
        description: 'Pierwszym najważniejszym krokiem w podjęciu współpracy jest dokładne określenie potrzeb klienta oraz wstępne wytypowanie obszarów w jakich możemy pomóc.  Na tym etapie jest organizowane spotkanie z osobami zarządzającymi oraz jeśli jest taka potrzeba wszystkimi interesariuszami projektu. Pierwszy krok kończy się podsumowaniem przedstawiającym możliwości działań.'
      },
      {
        title: 'OKREŚLENIE CELÓW',
        description: 'Drugie spotkanie jest poświęcone przede wszystkim określeniu celu biznesowego danego klienta, oczekiwanych rezultatów oraz wskaźników na podstawie , których monitorujemy postępy w realizacji danego projektu. W tym kroku także  powstaje zarys proponowanych działań i etapów wdrożenia.'
      },
      {
        title: 'Audyt',
        description: 'W momencie podjęcia realizacji i jeśli projekt tego wymaga robimy analizę stanu obecnego u klienta tak aby jak najlepiej dobrać odpowiednie narzędzia i zespół do realizacji celów ustalonych na wcześniejszych spotkaniach. Jeśli w ramach wdrożenia jest to wymagane wykonujemy usługę „tajemniczego klienta” aby sprawdzić procesy obecnie zachodzące w przedsiębiorstwie.'
      },
      {
        title: 'OPRACOWANIE STRATEGII',
        description: 'W momencie kiedy posiadamy pełny obraz przedsiębiorstwa i określone wszystkie kluczowe wskaźniki, przystępujemy do wykonania strategii najbliższych działań, zarówno w obszarze czynności operacyjnych jak i marketingowych. Strategia uwzględnia zasoby klienta oraz zasoby Grupy Hiteger odpowiednio określając zakres zangażowania.'
      },
      {
        title: 'REALIZACJA',
        description: 'Wykonujemy pełne wdrożenie u klienta, wcześniej opracowanej strategii i działań projektowych. Szkolimy pracowników, wdrażamy narzędzia i osiągamy zamierzone cele. Na tym etapie odpowiednio dopasowujemy kompetencje naszego zespołu tak aby jak najlepiej pomóc naszym klientom w realizacji ich założeń biznesowych.'
      },
      {
        title: 'WSPARCIE I OPTYMALIZACJA',
        description: 'Naszym celem jest aby jak najlepiej zrealizować oczekiwania naszych klientów, związku z powyższym nie zostawiamy bez opieki nikogo jeśli wdrożone procesy i procedury wymagają jeszcze wsparcia. Każdy nasz klient ma dostęp do  naszego zespołu po okresie wdrożeniowym.'
      }
    ]
  }
})
var success = new Vue({el:'.success'})
var team = new Vue({el:'.team'})
var blog = new Vue({el:'.blog'})
var services = new Vue({el:'.services'})
var numbers = new Vue({
  el:'#numbers',
  data: {
    hexagons: [
      {
        number: 7,
        text: 'Zadowolonych klientów'
      },
      {
        number: 128,
        text: 'Zadowolonych klientów'
      },
      {
        number: 1826,
        text: 'Zadowolonych klientów'
      },
      {
        number: 1826,
        text: 'Zadowolonych klientów'
      }
    ]
  }
})

var service = new Vue({
  el:'#service',
  data: function() {
    return {
      service: null,
      activeOdd: 0,
      activeEven: 1,
      active: 0,
      services: main.items[1].children,
      toggle: true,
      sideActive: 13
    }
  },
  beforeCreate () {
    axios
      .get('https://hiteger.ideaware.pl/wp-json/wp/v2/services')
      .then(function(response) {
        service.service = response.data[0]
        console.log(service.service)
      })
  },
  computed: {
    itemsEven () {
      return this.service.acf.zakres_dzialan.body.filter((el, index, arr) => {
        return (index % 2 === 0)
      })
    },
    itemsOdd () {
      return this.service.acf.zakres_dzialan.body.filter((el, index, arr) => {
        return (index % 2 === 1)
      })
    }
  }
})

var footer = new Vue({
  el:'footer',
  data: {
    navigation: [
      'Home',
      'Kompetencje',
      'Usługi',
      'Branże',
      'O nas',
      'Kontakt'
    ],
    services: [
      'Audyty i analizy',
      'Doradztwo biznesowe',
      'Projekty strategiczne',
      'Programy rozwojowe',
      'Interim manager',
      'Wdrożenia / Narzędzia',
    ],
    informations: [
      'Polityka prywatności',
      'RODO',
      'Mapa Strony',
      'Kariera',
      'Blog'
    ],
    contact: [
      {
        title:'Biuro',
        email:'biuro@hiteger.pl',
        phone:'00 000 00 00'
      },
      {
        title:'Sprzedaż',
        email:'sprzedaz@hiteger.pl',
        phone:'00 000 00 00'
      },
      {
        title:'Księgowość',
        email:'ksiegowosc@hiteger.pl',
        phone:'00 000 00 00'
      },
    ]
  }
})

var SwiperMission = new Swiper ('.our-mission__slider', {
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
})
var SwiperTestimonials = new Swiper ('.swiper-testimonials', {
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
})
var SwiperLogos = new Swiper ('.swiper-logos', {
  loop: true,
  slidesPerView: 'auto',
  loopedSlides: 5,
  autoplay: {
    delay: 1500,
    disableOnInteraction: false,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
})
