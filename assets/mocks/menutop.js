export default {
  'ID': 3,
  'name': 'Menu Top',
  'slug': 'menu-top',
  'description': '',
  'count': 4,
  'items': [
    {
      'id': 40,
      'order': 1,
      'parent': 0,
      'title': 'Blog',
      'url': 'http://hiteger.dev.cc/blog/',
      'attr': '',
      'target': '',
      'classes': '',
      'xfn': '',
      'description': '',
      'object_id': 9,
      'object': 'page',
      'object_slug': 'blog',
      'type': 'post_type',
      'type_label': 'Strona',
      'toggled': false
    },
    {
      'id': 41,
      'order': 2,
      'parent': 0,
      'title': 'Case Studies',
      'url': 'http://#',
      'attr': '',
      'target': '',
      'classes': '',
      'xfn': '',
      'description': '',
      'object_id': 41,
      'object': 'custom',
      'object_slug': 'case-studies',
      'type': 'custom',
      'type_label': 'Własny odnośnik',
      'toggled': false
    },
    {
      'id': 42,
      'order': 3,
      'parent': 0,
      'title': 'FAQ',
      'url': 'http://#',
      'attr': '',
      'target': '',
      'classes': '',
      'xfn': '',
      'description': '',
      'object_id': 42,
      'object': 'custom',
      'object_slug': 'faq',
      'type': 'custom',
      'type_label': 'Własny odnośnik',
      'toggled': false
    },
    {
      'id': 43,
      'order': 4,
      'parent': 0,
      'title': 'Kariera',
      'url': 'http://#',
      'attr': '',
      'target': '',
      'classes': '',
      'xfn': '',
      'description': '',
      'object_id': 43,
      'object': 'custom',
      'object_slug': 'kariera',
      'type': 'custom',
      'type_label': 'Własny odnośnik',
      'toggled': false
    }
  ],
  'meta': {
    'links': {
      'collection': 'http://hiteger.dev.cc/wp-json/wp/v2/menus/',
      'self': 'http://hiteger.dev.cc/wp-json/wp/v2/menus/3'
    }
  }
}
